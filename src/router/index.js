import Vue from 'vue'
import Router from 'vue-router'
import DownLoad from '@/components/DownLoad'
import GoEat from '@/components/BigMeal/GoEat'
import FindFriend from '@/components/FindFriend/FindFriend'
import GoExercise from '@/components/BigMeal/GoExercise'
import WeekReport from '@/components/Report'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'DownLoad',
      component: DownLoad
    },
    {
      path: '/goeat/:id',
      name: 'Eat',
      component: GoEat
    },{
      path: '/findfriend/:id',
      name: 'Find',
      component: FindFriend
    },
    {
      path: '/exercise/:id',
      name: 'Exercise',
      component: GoExercise
    },
    {
      path: '/report',
      name: 'Report',
      component: WeekReport
    }
  ]
})

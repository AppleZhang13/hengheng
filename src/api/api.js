import service from './request'
export function  getPersonInfo (data) {
    return service({
        url: '/api/linkInfo',
        method: 'post',
        data
    })
};
export function  toEat (data) {
    return service({
        url: '/api/doEat',
        method: 'post',
        data
    })
};
export function  findFriend (data) {
    return service({
        url: '/api/doFind',
        method: 'post',
        data
    })
};
///api/placeList
export function  placeList (data) {
    return service({
        url: '/api/placeList',
        method: 'post',
        data
    })
};
export function  doSports (data) {
    return service({
        url: '/api/doSport',
        method: 'post',
        data
    })
};
export function  weekly (data) {
    return service({
        url: '/api/weekly',
        method: 'post',
        data
    })
};
export function  getwx (params) {
    return service({
        url: '/api/WechatAuthUrl',
        method: 'get',
        params
    })
};
///api/WechatAuth
export function userInfo(params){
    return service({
        url: '/api/WechatAuth',
        method: 'get',
        params
    })
}
